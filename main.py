import sys
import os
import subprocess as sp
#from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QLabel,
                             QPushButton, QFileDialog, QLineEdit, QTabWidget,
                                QGridLayout, QVBoxLayout, QHBoxLayout,
                             QTableWidget, QTableWidgetItem, QProgressBar,
                             QFileDialog,
                               )
from PyQt5.QtGui import QPalette, QColor
from PyQt5 import QtGui
from PyQt5.QtCore import QProcess

class Window(QMainWindow):
        # главная функция с GUI
        def __init__(self):
                super().__init__()              # вызывает конструктор базового класса
                self.title = "Редактор текстового файла"
                self.left = 50
                self.top = 50
                self.width = 650
                self.height = 500
                #
                #self.setStyleSheet('background-color:grey')
                self.show_gui()


        def show_gui(self):
                self.setGeometry(self.left, self.top, self.width, self.height)
                self.setWindowTitle(self.title)

                #vbox = QVBoxLayout(self)
                tabs = QTabWidget(self)
                tabs.setFont(QtGui.QFont("Sanserif", 12))
                #tabs.setTabPosition(QTabWidget.West)
                #tabs.setMovable(True)
                tabs.addTab(TabSysInfo(), 'Системная информация')
                tabs.addTab(TabProcesses(), 'Процессы')
                tabs.resize(self.width - 5, self.height - 5)

                #TabSysInfo.lbl11.setText(self.get_info())

                #widget1 = Color('blue')
                #self.setCentralWidget(widget1)

                self.show()
                


class Color(QWidget):

    def __init__(self, color):
        super(Color, self).__init__()
        self.setAutoFillBackground(True)

        palette = self.palette()
        palette.setColor(QPalette.ColorRole.Window, QColor(color))
        self.setPalette(palette)

class TabSysInfo(QWidget):
        def __init__(self):
                super().__init__()
                # графические элементы - Название
                lbl1 = QLabel(self)
                lbl1.setText("Имя компьютера: ")
                lbl2 = QLabel(self)
                lbl2.setText('Пользователь: ')
                lbl3 = QLabel(self)
                lbl3.setText("Uptime: ")
                lbl4 = QLabel(self)
                lbl4.setText('Процессор: ')
                lbl5 = QLabel(self)
                lbl5.setText("Частота: ")
                lbl6 = QLabel(self)
                lbl6.setText('Загрузка ЦП: ')
                lbl7 = QLabel(self)
                lbl7.setText("Оперативная память: ")
                lbl8 = QLabel(self)
                lbl8.setText('Использование ОЗУ: ')

                # графические элементы - Состояние
                self.lbl11 = QLabel(self)
#                lbl11.setText(str(self.get_info))   # - ?
#                lbl11.setText("test Hostname")
                lbl12 = QLabel(self)
                lbl12.setText(str(os.environ.get('USERNAME')))
#                lbl12.setText(str(getpass.getuser()))
                self.lbl13 = QLabel(self)
                #lbl13.setText()
                self.lbl14 = QLabel(self)
                #lbl4.setText('Процессор: ')
                self.lbl15 = QLabel(self)
                #lbl5.setText("Частота: ")
#                lbl16 = QLabel(self)
                self.pbar_cpu = QProgressBar(self)
                self.pbar_cpu.setValue(0)
                #lbl6.setText('Загрузка ЦП: ')
                self.lbl17 = QLabel(self)
                #lbl7.setText("Оперативная память: ")
#                lbl18 = QLabel(self)
                self.pbar_mem = QProgressBar(self)
#                self.pbar_mem.setValue(0)
                #lbl8.setText('Использование ОЗУ: ')
                # разметка
                #layout1.setContentsMargins(0,0,0,0)
                #layout1.setSpacing(20)
                hbox_center = QHBoxLayout(self)
                vbox_left = QVBoxLayout(self)
                vbox_right = QVBoxLayout(self)
                
                vbox_left.addWidget(lbl1)
                vbox_left.addWidget(lbl2)
                vbox_left.addWidget(lbl3)
                vbox_left.addWidget(lbl4)
                vbox_left.addWidget(lbl5)
                vbox_left.addWidget(lbl6)
                vbox_left.addWidget(lbl7)
                vbox_left.addWidget(lbl8)

                vbox_right.addWidget(self.lbl11)
                vbox_right.addWidget(lbl12)
                vbox_right.addWidget(self.lbl13)
                vbox_right.addWidget(self.lbl14)
                vbox_right.addWidget(self.lbl15)
                vbox_right.addWidget(self.pbar_cpu)
                vbox_right.addWidget(self.lbl17)
                vbox_right.addWidget(self.pbar_mem)

                hbox_center.addLayout(vbox_left)
                hbox_center.addLayout(vbox_right)
                self.setLayout(hbox_center)
                vbox_right = QVBoxLayout(self)

                self.parse_uptime()
                self.get_cpu_info()
                self.get_mem_info()
                self.get_hostname()

        # --------------- FUNCTIONS ----------------------

        def get_hostname(self):
                with open("/proc/sys/kernel/hostname", 'r') as f:
                        hostname = f.read()
                        
#                return hostname
                        self.lbl11.setText(hostname.strip("\n"))

        def parse_uptime(self):
            with open('/proc/uptime', 'r') as f:
                uptime_seconds = float(f.readline().split()[0])
            min = uptime_seconds/60
            if int(int(min)/60) < 24:
                h = int(int(min)/60)
                m = int(int(min)-h*60)
#                print(str("{}h {}m".format(h,m)))
                self.lbl13.setText(str("{}h {}m".format(h,m)))
            else:
                d = int(int(min)/1440)
                n = int(int(min) - 60*24)
                h = int(int(min)/60)
                m = int(int(min)-h*60)
                self.lbl13.setText(str("{}h {}m".format(h,m)))
#                print(str("более суток {}d  {}h {}m".format(d,h,m)))

        def get_cpu_info(self):
            os.chdir("/proc/")
            with open("cpuinfo", 'r') as f:
                cpuinfo = f.read()
            for line in cpuinfo.split("\n"):
                if "model name" in line:
                    model = line.split(":")[1]
                    #print(model.split("@")[0])
                    #print(model.split("@")[1])
                    self.lbl14.setText(model.split("@")[0])
                    self.lbl15.setText(model.split("@")[1])

        def get_mem_info(self):
            os.chdir("/proc/")
            with open("meminfo", 'r') as f:
                meminfo = f.read()
            for line in meminfo.split("\n"):
                if "MemTotal:" in line:
                    num = [int(x) for x in line.split() if x.isdigit()]
                    str1 = num[0]
                    total = int(int(num[0])/1024)
                if "MemFree:" in line:
                    num = [int(x) for x in line.split() if x.isdigit()]
                    free = int(int(num[0])/1024)
                    used = total - free
                    percent_in_use = int(used/(total/100))
                    percent_free = int(free/(total/100))
                    self.lbl17.setText(f'Всего: {total}, исп: {used}, своб: {free}')
                    self.pbar_mem.setValue(percent_in_use)



class TabProcesses(QWidget):
        def __init__(self):
                super().__init__()
                '''lbl1 = QLabel(self)
                lbl1.setText("Все процессы: ")
                lbl2 = QLabel(self)
                lbl2.setText('Инфо: ')'''
                self.data = []	# list for list_of_pids
                # графические элементы
                self.table = QTableWidget(self)
                self.table.setColumnCount(5)
                self.table.cellClicked.connect(self.get_table_item)

                #table.setRowCount(0);
#                self.table.horizontalHeaderItem(0).setToolTip("PID - ИД процесса")
#                self.table.horizontalHeaderItem(1).setToolTip("Название процесса")
                self.table.setHorizontalHeaderLabels(["PID", "Название процесса", "Статус", "Приоритет", "cmd"])

                self.lbl_info = QLabel(self)
                self.lbl_info.setText('Выбранный процесс: ')
                self.lbl2_info = QLabel(self)
                self.lbl2_info.setText(' - ')

                btn_save = QPushButton(self)
                btn_save.setText('Сохранить')
                btn_save.clicked.connect(self.save_list_of_pids)
                btn_kill = QPushButton(self)
                btn_kill.setText('Завершить')
                btn_kill.clicked.connect(self.kill_selected_pid)
                btn_kill.setToolTip("Для завершения процесса вы должны выделить его PID и нажать на кнопку \"Завершить\"")
                # разметка
                vbox = QVBoxLayout(self)
                hbox = QHBoxLayout(self)
                hbox.addStretch()
                hbox.addWidget(btn_save)
                hbox.addWidget(self.lbl_info)
                hbox.addWidget(self.lbl2_info)
               # hbox.addWidget(btn_save)
                hbox.addWidget(btn_kill)
                
                vbox.addWidget(self.table)
                vbox.addLayout(hbox)
                self.setLayout(vbox)

                # ------ RUN
                self.list_of_pids_and_processes()

               # ----------------- FUNCTIONS -----------
        def list_of_pids_and_processes(self):
            spisok = []
            process = []
            path = "/proc"
            os.chdir(path)
            for dir in os.listdir('.'):
                if os.path.isdir(dir) & dir.isdigit():
                    spisok+=[dir]
            for (dirs) in spisok:
#                with open("/proc/" + dirs + "/comm", 'r') as f:
            #  create new row
                rowPos = self.table.rowCount()
                self.table.insertRow(rowPos)
               # ------  PID
                self.table.setItem( rowPos, 0, QTableWidgetItem(dirs))
               # ------ process name
                with open("/proc/" + dirs + "/comm", 'r') as f:
                    self.table.setItem( rowPos, 1, QTableWidgetItem(f.read().rstrip('\n')))
               # ------ process status
                with open("/proc/" + dirs + "/status", "r") as f:
                    for line in f.readlines():
                        if line.lower().startswith('state'):
                            self.table.setItem( rowPos, 2, QTableWidgetItem(line.split(":")[1].replace("\t","")))
               # ---- priority
                with open("/proc/" + dirs + "/stat", "r") as f:
                    self.table.setItem( rowPos, 3, QTableWidgetItem(f.read().split(" ")[18]))
                # ---- command line
                with open("/proc/" + dirs + "/cmdline", 'r') as f:
                    self.table.setItem( rowPos, 4, QTableWidgetItem(f.read().rstrip('\n')))

                self.table.resizeColumnsToContents()

        def get_table_item(self, sig):
            row = self.table.currentItem().row()
            #self.lbl_info.setText(str(row))
            #
            colcount = self.table.columnCount()
            for i in range(0, colcount):
                cell = self.table.item(row, i).text()
                #return cell
                self.lbl2_info.setText(cell)
            #cell = self.table.item(row, self.table.columnCount()).text()
            #self.lbl2_info.setText(cell)
            
        def save_list_of_pids(self):
            self.get_items()
            #print(self.data)
            try:
                file_open = QFileDialog.getSaveFileName(self, caption='Open file', directory='./')[0]
                with open(file_open, 'a') as f:
                    for item in self.data:
                        f.write(str(item))
            except Exception as e:
                print ("Ошибка: " + str(e))
            if not file_open:
                return

        def kill_selected_pid(self):
            sp.run(['pkill', '-f', self.lbl2_info.text(), '-u', os.environ.get('USERNAME')])
            self.list_of_pids_and_processes()
            self.lbl2_info.setText("Успешно!")

        def get_items(self):
            for row in range(self.table.rowCount()):
                listok = []
                for column in range(self.table.columnCount()):
                    listok.append(self.table.item(row, column).text())
                    #self.data.append(self.table.item(column, row).text())
                    #self.table.item(row, i).text()
                self.data.append(listok)
#                return data
            #for stroka in data: return stroka
                    #return (self.table.item(column, row).text())

        def add_item(self):
#  create new row
            rowPos = self.table.rowCount()
            self.table.insertRow(rowPos)
# insert TEXT into row
            self.table.setItem( rowPos, 0, QTableWidgetItem("column 1"))
            self.table.setItem( rowPos, 1, QTableWidgetItem("column 2"))



# -------------- RUN -------------
if __name__ == '__main__':
        app = QApplication(sys.argv)
        ex = Window()
        sys.exit(app.exec_())

print (f"__name__={__name__}")
