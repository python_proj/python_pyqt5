#!/bin/bash
## ВНИМАНИЕ!!! перед запуском конвертировать утилитой dos2unix

# checking for ROOT
if [ "$(whoami)" != 'root']; then
	echo " YOU are NOT fucking ROOT !!!"
	echo " RUN this script with SUDO ! "
	exit 1;
fi

#
IMAGE=ubuntu-20.04.2-live-server-amd64.iso
BUILD_DIR='/tmp/build'
CUR_DIR=$(cd $(dirname $0) && pwd) # dir with script
#
if [ -d "$BUILD_DIR" ]; then
	rm -rf $BUILD_DIR	&& echo "--- папки удалены" # удалить уже имеющиеся файлы и папки без запросов
else
	echo " NO dirs found"
fi

# создаём папку для извлечения
sudo mkdir -p $BUILD_DIR/nocloud && echo "--- папки созданы"

# извлекаем образ
xorriso -osirrox on -indev $IMAGE -extract / $BUILD_DIR && chmod -R +w $BUILD_DIR && echo "--- образ извлечён"

# создаём пустой мета файл
touch $BUILD_DIR/nocloud/meta-data

# копируем файл с юзер-дата (преднастройки)
cp user-data $BUILD_DIR/nocloud/user-data && echo "--- дата-файлы скопированы"

# MAKE and COPY empty directory EXTRA
mkdir -p $BUILD_DIR/pool/extra && echo "--- EXTRA created succesfully"
cp -R $CUR_DIR/extra/* $BUILD_DIR/pool/extra

# обновляем флаги загрузки с новыми данными
sed -i 's|---|autoinstall ds=nocloud\\\;s=/cdrom/nocloud/ ---|g' $BUILD_DIR/boot/grub/grub.cfg
sed -i 's|---|autoinstall ds=nocloud;s=/cdrom/nocloud/ ---|g' $BUILD_DIR/isolinux/txt.cfg
echo ">>> файл автозагрузки обновлён"

# МД5сум делаем
echo ">>> calc MD5 sum..."
#rm $BUILD_DIR/md5sum.txt
#(cd $BUILD_DIR && find . -type f -print0 | xargs -0 md5sum | grep -v "boot.cat" | grep -v "md5sum.txt" > md5sum.txt)

mv $BUILD_DIR/ubuntu .
(cd $BUILD_DIR ; find '!' -name "md5sum.txt" '!' -path "./isolinux/*" -follow -type f -exec "$(which md5sum)" {} \; > ../md5sum.txt.NEW)

mv /tmp/md5sum.txt.NEW $BUILD_DIR/md5sum.txt
mv ubuntu $BUILD_DIR
echo ">>> МД5 пересчитана"
echo ">>> .........."
echo ">>> Нужно запустить следующий скрипт makeiso.sh"
